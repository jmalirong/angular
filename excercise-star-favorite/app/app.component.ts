import {Component} from 'angular2/core';
import {FavoriteComponent} from './favorite.component';
import {HeartComponent} from './heart.component';

@Component({
    selector: 'my-app',
    template: ` 
    	<i class="glyphicon glyphicon-star"></i>
    			<favorite [isFavorite]="post.isFavorite" (change)="onFavoriteChange($event)"></favorite>
    			<heart [totalLikes]="tweet.totalLikes" [iLike]="tweet.iLike" ></heart>

    			`,
    directives: [FavoriteComponent,HeartComponent]
})
export class AppComponent { 
	post = {
		title: "Title",
		isFavorite: true
	}

	onFavoriteChange($event){
		console.log($event);
	}

	tweet = {
		totalLikes: 10,
		iLike: false
	}
}