System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            //first button is class binding
            //second button is style binding
            AppComponent = (function () {
                function AppComponent() {
                    this.title = "Angular App";
                    this.isActive = true;
                }
                AppComponent.prototype.onDivClick = function () {
                    console.log("Handled by Div");
                };
                AppComponent.prototype.onClick = function ($event) {
                    $event.stopPropagation(); //to stop bubbling, try to remove this to see the difference and show console.log
                    console.log("clicked", $event);
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n    \t<div>\n    \t\t<h2>Class Binding</h2>\n    \t\t<button class=\"btn btn-primary\" [class.active]=\"isActive\">Class Binding</button>\n    \t</div>\n    \t<div>\n    \t\t<h2>Style Binding</h2>\n    \t\t<button class=\"btn btn-primary\" [style.backgroundColor]=\"isActive ? 'blue' : 'gray' \">Style Binding</button>\n    \t</div>\n    \t\n    \t<div (click)=\"onDivClick()\">\n    \t\t<button (click)=\"onClick($event)\" class=\"btn btn-danger\">Event binding</button>\n    \t</div>\n    \t<div>\n\t\t\t<h2>Two way binding</h2>\n\t\t\t<input type=\"text\" [(ngModel)]=\"title\">\n\t\t\t<input type=\"button\" (click)=\"title = ''\" value=\"Clear\" />\n\t\t\tPreview: {{ title }}\n    \t</div>\n    \t<div>\n\t\t\t<h2>Star Practice</h2>\n\t\t\t\n    \t</div>\n\n    \t"
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map