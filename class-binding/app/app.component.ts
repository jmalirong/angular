import {Component} from 'angular2/core';

//first button is class binding
//second button is style binding

@Component({
    selector: 'my-app',
    template: `
    	<div>
    		<h2>Class Binding</h2>
    		<button class="btn btn-primary" [class.active]="isActive">Class Binding</button>
    	</div>
    	<div>
    		<h2>Style Binding</h2>
    		<button class="btn btn-primary" [style.backgroundColor]="isActive ? 'blue' : 'gray' ">Style Binding</button>
    	</div>
    	
    	<div (click)="onDivClick()">
    		<button (click)="onClick($event)" class="btn btn-danger">Event binding</button>
    	</div>
    	<div>
			<h2>Two way binding</h2>
			<input type="text" [(ngModel)]="title">
			<input type="button" (click)="title = ''" value="Clear" />
			Preview: {{ title }}
    	</div>
    	<div>
			<h2>Star Practice</h2>
			
    	</div>

    	`
})
export class AppComponent { 
	title = "Angular App";

	isActive = true;

	onDivClick(){
		console.log("Handled by Div");
	}

	onClick($event){
		$event.stopPropagation(); //to stop bubbling, try to remove this to see the difference and show console.log
		console.log("clicked",$event);
	}
}